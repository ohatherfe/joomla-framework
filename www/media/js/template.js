/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (function() { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./assets/js/template.js":
/*!*******************************!*\
  !*** ./assets/js/template.js ***!
  \*******************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var smooth_scroll__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! smooth-scroll */ \"./node_modules/smooth-scroll/dist/smooth-scroll.polyfills.min.js\");\n/* harmony import */ var smooth_scroll__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(smooth_scroll__WEBPACK_IMPORTED_MODULE_0__);\n\nnew (smooth_scroll__WEBPACK_IMPORTED_MODULE_0___default())('[data-scroll]', {\n  header: '[data-scroll-header]',\n  speed: 600\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9hc3NldHMvanMvdGVtcGxhdGUuanMuanMiLCJtYXBwaW5ncyI6Ijs7O0FBQUE7QUFFQSxJQUFJQSxzREFBSixDQUFpQixlQUFqQixFQUFrQztFQUM5QkMsTUFBTSxFQUFFLHNCQURzQjtFQUU5QkMsS0FBSyxFQUFFO0FBRnVCLENBQWxDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL3RlbXBsYXRlLmpzPzI2YjYiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFNtb290aFNjcm9sbCBmcm9tICdzbW9vdGgtc2Nyb2xsJztcblxubmV3IFNtb290aFNjcm9sbCgnW2RhdGEtc2Nyb2xsXScsIHtcbiAgICBoZWFkZXI6ICdbZGF0YS1zY3JvbGwtaGVhZGVyXScsXG4gICAgc3BlZWQ6IDYwMCxcbn0pO1xuIl0sIm5hbWVzIjpbIlNtb290aFNjcm9sbCIsImhlYWRlciIsInNwZWVkIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./assets/js/template.js\n");

/***/ }),

/***/ "./assets/scss/template.scss":
/*!***********************************!*\
  !*** ./assets/scss/template.scss ***!
  \***********************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n// extracted by mini-css-extract-plugin\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9hc3NldHMvc2Nzcy90ZW1wbGF0ZS5zY3NzLmpzIiwibWFwcGluZ3MiOiI7QUFBQSIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL2Fzc2V0cy9zY3NzL3RlbXBsYXRlLnNjc3M/Yjc1YyJdLCJzb3VyY2VzQ29udGVudCI6WyIvLyBleHRyYWN0ZWQgYnkgbWluaS1jc3MtZXh0cmFjdC1wbHVnaW5cbmV4cG9ydCB7fTsiXSwibmFtZXMiOltdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./assets/scss/template.scss\n");

/***/ }),

/***/ "./assets/css/debugbar.css":
/*!*********************************!*\
  !*** ./assets/css/debugbar.css ***!
  \*********************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n// extracted by mini-css-extract-plugin\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9hc3NldHMvY3NzL2RlYnVnYmFyLmNzcy5qcyIsIm1hcHBpbmdzIjoiO0FBQUEiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvY3NzL2RlYnVnYmFyLmNzcz84Y2M4Il0sInNvdXJjZXNDb250ZW50IjpbIi8vIGV4dHJhY3RlZCBieSBtaW5pLWNzcy1leHRyYWN0LXBsdWdpblxuZXhwb3J0IHt9OyJdLCJuYW1lcyI6W10sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./assets/css/debugbar.css\n");

/***/ }),

/***/ "./node_modules/smooth-scroll/dist/smooth-scroll.polyfills.min.js":
/*!************************************************************************!*\
  !*** ./node_modules/smooth-scroll/dist/smooth-scroll.polyfills.min.js ***!
  \************************************************************************/
/***/ (function(module, exports, __webpack_require__) {

eval("var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*! smooth-scroll v16.1.3 | (c) 2020 Chris Ferdinandi | MIT License | http://github.com/cferdinandi/smooth-scroll */\nwindow.Element&&!Element.prototype.closest&&(Element.prototype.closest=function(e){var t,n=(this.document||this.ownerDocument).querySelectorAll(e),o=this;do{for(t=n.length;0<=--t&&n.item(t)!==o;);}while(t<0&&(o=o.parentElement));return o}),(function(){if(\"function\"==typeof window.CustomEvent)return;function e(e,t){t=t||{bubbles:!1,cancelable:!1,detail:void 0};var n=document.createEvent(\"CustomEvent\");return n.initCustomEvent(e,t.bubbles,t.cancelable,t.detail),n}e.prototype=window.Event.prototype,window.CustomEvent=e})(),(function(){for(var r=0,e=[\"ms\",\"moz\",\"webkit\",\"o\"],t=0;t<e.length&&!window.requestAnimationFrame;++t)window.requestAnimationFrame=window[e[t]+\"RequestAnimationFrame\"],window.cancelAnimationFrame=window[e[t]+\"CancelAnimationFrame\"]||window[e[t]+\"CancelRequestAnimationFrame\"];window.requestAnimationFrame||(window.requestAnimationFrame=function(e,t){var n=(new Date).getTime(),o=Math.max(0,16-(n-r)),a=window.setTimeout((function(){e(n+o)}),o);return r=n+o,a}),window.cancelAnimationFrame||(window.cancelAnimationFrame=function(e){clearTimeout(e)})})(),(function(e,t){ true?!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = (function(){return t(e)}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),\n\t\t__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)):0})(\"undefined\"!=typeof __webpack_require__.g?__webpack_require__.g:\"undefined\"!=typeof window?window:this,(function(M){\"use strict\";var q={ignore:\"[data-scroll-ignore]\",header:null,topOnEmptyHash:!0,speed:500,speedAsDuration:!1,durationMax:null,durationMin:null,clip:!0,offset:0,easing:\"easeInOutCubic\",customEasing:null,updateURL:!0,popstate:!0,emitEvents:!0},I=function(){var n={};return Array.prototype.forEach.call(arguments,(function(e){for(var t in e){if(!e.hasOwnProperty(t))return;n[t]=e[t]}})),n},r=function(e){\"#\"===e.charAt(0)&&(e=e.substr(1));for(var t,n=String(e),o=n.length,a=-1,r=\"\",i=n.charCodeAt(0);++a<o;){if(0===(t=n.charCodeAt(a)))throw new InvalidCharacterError(\"Invalid character: the input contains U+0000.\");1<=t&&t<=31||127==t||0===a&&48<=t&&t<=57||1===a&&48<=t&&t<=57&&45===i?r+=\"\\\\\"+t.toString(16)+\" \":r+=128<=t||45===t||95===t||48<=t&&t<=57||65<=t&&t<=90||97<=t&&t<=122?n.charAt(a):\"\\\\\"+n.charAt(a)}return\"#\"+r},F=function(){return Math.max(document.body.scrollHeight,document.documentElement.scrollHeight,document.body.offsetHeight,document.documentElement.offsetHeight,document.body.clientHeight,document.documentElement.clientHeight)},L=function(e){return e?(t=e,parseInt(M.getComputedStyle(t).height,10)+e.offsetTop):0;var t},x=function(e,t,n){0===e&&document.body.focus(),n||(e.focus(),document.activeElement!==e&&(e.setAttribute(\"tabindex\",\"-1\"),e.focus(),e.style.outline=\"none\"),M.scrollTo(0,t))},H=function(e,t,n,o){if(t.emitEvents&&\"function\"==typeof M.CustomEvent){var a=new CustomEvent(e,{bubbles:!0,detail:{anchor:n,toggle:o}});document.dispatchEvent(a)}};return function(o,e){var b,a,A,O,C={};C.cancelScroll=function(e){cancelAnimationFrame(O),O=null,e||H(\"scrollCancel\",b)},C.animateScroll=function(a,r,e){C.cancelScroll();var i=I(b||q,e||{}),c=\"[object Number]\"===Object.prototype.toString.call(a),t=c||!a.tagName?null:a;if(c||t){var s=M.pageYOffset;i.header&&!A&&(A=document.querySelector(i.header));var n,o,u,l,m,d,f,h,p=L(A),g=c?a:(function(e,t,n,o){var a=0;if(e.offsetParent)for(;a+=e.offsetTop,e=e.offsetParent;);return a=Math.max(a-t-n,0),o&&(a=Math.min(a,F()-M.innerHeight)),a})(t,p,parseInt(\"function\"==typeof i.offset?i.offset(a,r):i.offset,10),i.clip),y=g-s,v=F(),w=0,S=(n=y,u=(o=i).speedAsDuration?o.speed:Math.abs(n/1e3*o.speed),o.durationMax&&u>o.durationMax?o.durationMax:o.durationMin&&u<o.durationMin?o.durationMin:parseInt(u,10)),E=function(e){var t,n,o;l||(l=e),w+=e-l,d=s+y*(n=m=1<(m=0===S?0:w/S)?1:m,\"easeInQuad\"===(t=i).easing&&(o=n*n),\"easeOutQuad\"===t.easing&&(o=n*(2-n)),\"easeInOutQuad\"===t.easing&&(o=n<.5?2*n*n:(4-2*n)*n-1),\"easeInCubic\"===t.easing&&(o=n*n*n),\"easeOutCubic\"===t.easing&&(o=--n*n*n+1),\"easeInOutCubic\"===t.easing&&(o=n<.5?4*n*n*n:(n-1)*(2*n-2)*(2*n-2)+1),\"easeInQuart\"===t.easing&&(o=n*n*n*n),\"easeOutQuart\"===t.easing&&(o=1- --n*n*n*n),\"easeInOutQuart\"===t.easing&&(o=n<.5?8*n*n*n*n:1-8*--n*n*n*n),\"easeInQuint\"===t.easing&&(o=n*n*n*n*n),\"easeOutQuint\"===t.easing&&(o=1+--n*n*n*n*n),\"easeInOutQuint\"===t.easing&&(o=n<.5?16*n*n*n*n*n:1+16*--n*n*n*n*n),t.customEasing&&(o=t.customEasing(n)),o||n),M.scrollTo(0,Math.floor(d)),(function(e,t){var n=M.pageYOffset;if(e==t||n==t||(s<t&&M.innerHeight+n)>=v)return C.cancelScroll(!0),x(a,t,c),H(\"scrollStop\",i,a,r),!(O=l=null)})(d,g)||(O=M.requestAnimationFrame(E),l=e)};0===M.pageYOffset&&M.scrollTo(0,0),f=a,h=i,c||history.pushState&&h.updateURL&&history.pushState({smoothScroll:JSON.stringify(h),anchor:f.id},document.title,f===document.documentElement?\"#top\":\"#\"+f.id),\"matchMedia\"in M&&M.matchMedia(\"(prefers-reduced-motion)\").matches?x(a,Math.floor(g),!1):(H(\"scrollStart\",i,a,r),C.cancelScroll(!0),M.requestAnimationFrame(E))}};var t=function(e){if(!e.defaultPrevented&&!(0!==e.button||e.metaKey||e.ctrlKey||e.shiftKey)&&\"closest\"in e.target&&(a=e.target.closest(o))&&\"a\"===a.tagName.toLowerCase()&&!e.target.closest(b.ignore)&&a.hostname===M.location.hostname&&a.pathname===M.location.pathname&&/#/.test(a.href)){var t,n;try{t=r(decodeURIComponent(a.hash))}catch(e){t=r(a.hash)}if(\"#\"===t){if(!b.topOnEmptyHash)return;n=document.documentElement}else n=document.querySelector(t);(n=n||\"#top\"!==t?n:document.documentElement)&&(e.preventDefault(),(function(e){if(history.replaceState&&e.updateURL&&!history.state){var t=M.location.hash;t=t||\"\",history.replaceState({smoothScroll:JSON.stringify(e),anchor:t||M.pageYOffset},document.title,t||M.location.href)}})(b),C.animateScroll(n,a))}},n=function(e){if(null!==history.state&&history.state.smoothScroll&&history.state.smoothScroll===JSON.stringify(b)){var t=history.state.anchor;\"string\"==typeof t&&t&&!(t=document.querySelector(r(history.state.anchor)))||C.animateScroll(t,null,{updateURL:!1})}};C.destroy=function(){b&&(document.removeEventListener(\"click\",t,!1),M.removeEventListener(\"popstate\",n,!1),C.cancelScroll(),O=A=a=b=null)};return (function(){if(!(\"querySelector\"in document&&\"addEventListener\"in M&&\"requestAnimationFrame\"in M&&\"closest\"in M.Element.prototype))throw\"Smooth Scroll: This browser does not support the required JavaScript methods and browser APIs.\";C.destroy(),b=I(q,e||{}),A=b.header?document.querySelector(b.header):null,document.addEventListener(\"click\",t,!1),b.updateURL&&b.popstate&&M.addEventListener(\"popstate\",n,!1)})(),C}}));//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvc21vb3RoLXNjcm9sbC9kaXN0L3Ntb290aC1zY3JvbGwucG9seWZpbGxzLm1pbi5qcy5qcyIsIm1hcHBpbmdzIjoiQUFBQTtBQUNBLG1GQUFtRix1RUFBdUUsR0FBRyxlQUFlLHNCQUFzQixHQUFHLGdDQUFnQyxTQUFTLGNBQWMsZ0RBQWdELGdCQUFnQixNQUFNLHdDQUF3QywwQ0FBMEMsOERBQThELHdEQUF3RCxnQkFBZ0IsNENBQTRDLDBDQUEwQyxrTEFBa0wsMEVBQTBFLGtGQUFrRixPQUFPLEtBQUssZUFBZSx3RUFBd0UsZ0JBQWdCLEVBQUUsbUJBQW1CLEtBQXFDLENBQUMsaUNBQU8sRUFBRSxtQ0FBRSxXQUFXLFlBQVk7QUFBQSxrR0FBRSxDQUFDLENBQWdFLENBQUMsc0JBQXNCLHFCQUFNLENBQUMscUJBQU0scURBQXFELGFBQWEsT0FBTyw2TkFBNk4sY0FBYyxTQUFTLDJEQUEyRCxnQkFBZ0IsK0JBQStCLFdBQVcsS0FBSyxlQUFlLG1DQUFtQyw2REFBNkQsTUFBTSxFQUFFLDRHQUE0RyxtTUFBbU0sWUFBWSxjQUFjLG9OQUFvTixlQUFlLHVFQUF1RSxNQUFNLG1CQUFtQiwySkFBMkoscUJBQXFCLG1EQUFtRCx5QkFBeUIsbUJBQW1CLG1CQUFtQixFQUFFLDRCQUE0QixxQkFBcUIsaUJBQWlCLDJCQUEyQixzREFBc0QsaUNBQWlDLGlCQUFpQixrQkFBa0IsaUZBQWlGLFNBQVMsb0JBQW9CLG1EQUFtRCxvREFBb0QsUUFBUSx1QkFBdUIsZ0NBQWdDLEVBQUUsa0VBQWtFLHFSQUFxUixVQUFVLHNzQkFBc3NCLG9CQUFvQiw4R0FBOEcsNENBQTRDLGlHQUFpRywyQ0FBMkMsZ09BQWdPLGtCQUFrQiw0UUFBNFEsUUFBUSxJQUFJLGdDQUFnQyxTQUFTLFlBQVksWUFBWSw0QkFBNEIsMkJBQTJCLGlDQUFpQywrRUFBK0Usc0RBQXNELHNCQUFzQiw4QkFBOEIsdURBQXVELHFDQUFxQyw0QkFBNEIsZUFBZSxxR0FBcUcsMkJBQTJCLHFHQUFxRyxhQUFhLElBQUkscUJBQXFCLHNIQUFzSCxtQkFBbUIsNk5BQTZOLHVCQUF1Qix3SkFBd0osT0FBTyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9zbW9vdGgtc2Nyb2xsL2Rpc3Qvc21vb3RoLXNjcm9sbC5wb2x5ZmlsbHMubWluLmpzP2E2YjUiXSwic291cmNlc0NvbnRlbnQiOlsiLyohIHNtb290aC1zY3JvbGwgdjE2LjEuMyB8IChjKSAyMDIwIENocmlzIEZlcmRpbmFuZGkgfCBNSVQgTGljZW5zZSB8IGh0dHA6Ly9naXRodWIuY29tL2NmZXJkaW5hbmRpL3Ntb290aC1zY3JvbGwgKi9cbndpbmRvdy5FbGVtZW50JiYhRWxlbWVudC5wcm90b3R5cGUuY2xvc2VzdCYmKEVsZW1lbnQucHJvdG90eXBlLmNsb3Nlc3Q9ZnVuY3Rpb24oZSl7dmFyIHQsbj0odGhpcy5kb2N1bWVudHx8dGhpcy5vd25lckRvY3VtZW50KS5xdWVyeVNlbGVjdG9yQWxsKGUpLG89dGhpcztkb3tmb3IodD1uLmxlbmd0aDswPD0tLXQmJm4uaXRlbSh0KSE9PW87KTt9d2hpbGUodDwwJiYobz1vLnBhcmVudEVsZW1lbnQpKTtyZXR1cm4gb30pLChmdW5jdGlvbigpe2lmKFwiZnVuY3Rpb25cIj09dHlwZW9mIHdpbmRvdy5DdXN0b21FdmVudClyZXR1cm47ZnVuY3Rpb24gZShlLHQpe3Q9dHx8e2J1YmJsZXM6ITEsY2FuY2VsYWJsZTohMSxkZXRhaWw6dm9pZCAwfTt2YXIgbj1kb2N1bWVudC5jcmVhdGVFdmVudChcIkN1c3RvbUV2ZW50XCIpO3JldHVybiBuLmluaXRDdXN0b21FdmVudChlLHQuYnViYmxlcyx0LmNhbmNlbGFibGUsdC5kZXRhaWwpLG59ZS5wcm90b3R5cGU9d2luZG93LkV2ZW50LnByb3RvdHlwZSx3aW5kb3cuQ3VzdG9tRXZlbnQ9ZX0pKCksKGZ1bmN0aW9uKCl7Zm9yKHZhciByPTAsZT1bXCJtc1wiLFwibW96XCIsXCJ3ZWJraXRcIixcIm9cIl0sdD0wO3Q8ZS5sZW5ndGgmJiF3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lOysrdCl3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lPXdpbmRvd1tlW3RdK1wiUmVxdWVzdEFuaW1hdGlvbkZyYW1lXCJdLHdpbmRvdy5jYW5jZWxBbmltYXRpb25GcmFtZT13aW5kb3dbZVt0XStcIkNhbmNlbEFuaW1hdGlvbkZyYW1lXCJdfHx3aW5kb3dbZVt0XStcIkNhbmNlbFJlcXVlc3RBbmltYXRpb25GcmFtZVwiXTt3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lfHwod2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZT1mdW5jdGlvbihlLHQpe3ZhciBuPShuZXcgRGF0ZSkuZ2V0VGltZSgpLG89TWF0aC5tYXgoMCwxNi0obi1yKSksYT13aW5kb3cuc2V0VGltZW91dCgoZnVuY3Rpb24oKXtlKG4rbyl9KSxvKTtyZXR1cm4gcj1uK28sYX0pLHdpbmRvdy5jYW5jZWxBbmltYXRpb25GcmFtZXx8KHdpbmRvdy5jYW5jZWxBbmltYXRpb25GcmFtZT1mdW5jdGlvbihlKXtjbGVhclRpbWVvdXQoZSl9KX0pKCksKGZ1bmN0aW9uKGUsdCl7XCJmdW5jdGlvblwiPT10eXBlb2YgZGVmaW5lJiZkZWZpbmUuYW1kP2RlZmluZShbXSwoZnVuY3Rpb24oKXtyZXR1cm4gdChlKX0pKTpcIm9iamVjdFwiPT10eXBlb2YgZXhwb3J0cz9tb2R1bGUuZXhwb3J0cz10KGUpOmUuU21vb3RoU2Nyb2xsPXQoZSl9KShcInVuZGVmaW5lZFwiIT10eXBlb2YgZ2xvYmFsP2dsb2JhbDpcInVuZGVmaW5lZFwiIT10eXBlb2Ygd2luZG93P3dpbmRvdzp0aGlzLChmdW5jdGlvbihNKXtcInVzZSBzdHJpY3RcIjt2YXIgcT17aWdub3JlOlwiW2RhdGEtc2Nyb2xsLWlnbm9yZV1cIixoZWFkZXI6bnVsbCx0b3BPbkVtcHR5SGFzaDohMCxzcGVlZDo1MDAsc3BlZWRBc0R1cmF0aW9uOiExLGR1cmF0aW9uTWF4Om51bGwsZHVyYXRpb25NaW46bnVsbCxjbGlwOiEwLG9mZnNldDowLGVhc2luZzpcImVhc2VJbk91dEN1YmljXCIsY3VzdG9tRWFzaW5nOm51bGwsdXBkYXRlVVJMOiEwLHBvcHN0YXRlOiEwLGVtaXRFdmVudHM6ITB9LEk9ZnVuY3Rpb24oKXt2YXIgbj17fTtyZXR1cm4gQXJyYXkucHJvdG90eXBlLmZvckVhY2guY2FsbChhcmd1bWVudHMsKGZ1bmN0aW9uKGUpe2Zvcih2YXIgdCBpbiBlKXtpZighZS5oYXNPd25Qcm9wZXJ0eSh0KSlyZXR1cm47blt0XT1lW3RdfX0pKSxufSxyPWZ1bmN0aW9uKGUpe1wiI1wiPT09ZS5jaGFyQXQoMCkmJihlPWUuc3Vic3RyKDEpKTtmb3IodmFyIHQsbj1TdHJpbmcoZSksbz1uLmxlbmd0aCxhPS0xLHI9XCJcIixpPW4uY2hhckNvZGVBdCgwKTsrK2E8bzspe2lmKDA9PT0odD1uLmNoYXJDb2RlQXQoYSkpKXRocm93IG5ldyBJbnZhbGlkQ2hhcmFjdGVyRXJyb3IoXCJJbnZhbGlkIGNoYXJhY3RlcjogdGhlIGlucHV0IGNvbnRhaW5zIFUrMDAwMC5cIik7MTw9dCYmdDw9MzF8fDEyNz09dHx8MD09PWEmJjQ4PD10JiZ0PD01N3x8MT09PWEmJjQ4PD10JiZ0PD01NyYmNDU9PT1pP3IrPVwiXFxcXFwiK3QudG9TdHJpbmcoMTYpK1wiIFwiOnIrPTEyODw9dHx8NDU9PT10fHw5NT09PXR8fDQ4PD10JiZ0PD01N3x8NjU8PXQmJnQ8PTkwfHw5Nzw9dCYmdDw9MTIyP24uY2hhckF0KGEpOlwiXFxcXFwiK24uY2hhckF0KGEpfXJldHVyblwiI1wiK3J9LEY9ZnVuY3Rpb24oKXtyZXR1cm4gTWF0aC5tYXgoZG9jdW1lbnQuYm9keS5zY3JvbGxIZWlnaHQsZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbEhlaWdodCxkb2N1bWVudC5ib2R5Lm9mZnNldEhlaWdodCxkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQub2Zmc2V0SGVpZ2h0LGRvY3VtZW50LmJvZHkuY2xpZW50SGVpZ2h0LGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGllbnRIZWlnaHQpfSxMPWZ1bmN0aW9uKGUpe3JldHVybiBlPyh0PWUscGFyc2VJbnQoTS5nZXRDb21wdXRlZFN0eWxlKHQpLmhlaWdodCwxMCkrZS5vZmZzZXRUb3ApOjA7dmFyIHR9LHg9ZnVuY3Rpb24oZSx0LG4pezA9PT1lJiZkb2N1bWVudC5ib2R5LmZvY3VzKCksbnx8KGUuZm9jdXMoKSxkb2N1bWVudC5hY3RpdmVFbGVtZW50IT09ZSYmKGUuc2V0QXR0cmlidXRlKFwidGFiaW5kZXhcIixcIi0xXCIpLGUuZm9jdXMoKSxlLnN0eWxlLm91dGxpbmU9XCJub25lXCIpLE0uc2Nyb2xsVG8oMCx0KSl9LEg9ZnVuY3Rpb24oZSx0LG4sbyl7aWYodC5lbWl0RXZlbnRzJiZcImZ1bmN0aW9uXCI9PXR5cGVvZiBNLkN1c3RvbUV2ZW50KXt2YXIgYT1uZXcgQ3VzdG9tRXZlbnQoZSx7YnViYmxlczohMCxkZXRhaWw6e2FuY2hvcjpuLHRvZ2dsZTpvfX0pO2RvY3VtZW50LmRpc3BhdGNoRXZlbnQoYSl9fTtyZXR1cm4gZnVuY3Rpb24obyxlKXt2YXIgYixhLEEsTyxDPXt9O0MuY2FuY2VsU2Nyb2xsPWZ1bmN0aW9uKGUpe2NhbmNlbEFuaW1hdGlvbkZyYW1lKE8pLE89bnVsbCxlfHxIKFwic2Nyb2xsQ2FuY2VsXCIsYil9LEMuYW5pbWF0ZVNjcm9sbD1mdW5jdGlvbihhLHIsZSl7Qy5jYW5jZWxTY3JvbGwoKTt2YXIgaT1JKGJ8fHEsZXx8e30pLGM9XCJbb2JqZWN0IE51bWJlcl1cIj09PU9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbChhKSx0PWN8fCFhLnRhZ05hbWU/bnVsbDphO2lmKGN8fHQpe3ZhciBzPU0ucGFnZVlPZmZzZXQ7aS5oZWFkZXImJiFBJiYoQT1kb2N1bWVudC5xdWVyeVNlbGVjdG9yKGkuaGVhZGVyKSk7dmFyIG4sbyx1LGwsbSxkLGYsaCxwPUwoQSksZz1jP2E6KGZ1bmN0aW9uKGUsdCxuLG8pe3ZhciBhPTA7aWYoZS5vZmZzZXRQYXJlbnQpZm9yKDthKz1lLm9mZnNldFRvcCxlPWUub2Zmc2V0UGFyZW50Oyk7cmV0dXJuIGE9TWF0aC5tYXgoYS10LW4sMCksbyYmKGE9TWF0aC5taW4oYSxGKCktTS5pbm5lckhlaWdodCkpLGF9KSh0LHAscGFyc2VJbnQoXCJmdW5jdGlvblwiPT10eXBlb2YgaS5vZmZzZXQ/aS5vZmZzZXQoYSxyKTppLm9mZnNldCwxMCksaS5jbGlwKSx5PWctcyx2PUYoKSx3PTAsUz0obj15LHU9KG89aSkuc3BlZWRBc0R1cmF0aW9uP28uc3BlZWQ6TWF0aC5hYnMobi8xZTMqby5zcGVlZCksby5kdXJhdGlvbk1heCYmdT5vLmR1cmF0aW9uTWF4P28uZHVyYXRpb25NYXg6by5kdXJhdGlvbk1pbiYmdTxvLmR1cmF0aW9uTWluP28uZHVyYXRpb25NaW46cGFyc2VJbnQodSwxMCkpLEU9ZnVuY3Rpb24oZSl7dmFyIHQsbixvO2x8fChsPWUpLHcrPWUtbCxkPXMreSoobj1tPTE8KG09MD09PVM/MDp3L1MpPzE6bSxcImVhc2VJblF1YWRcIj09PSh0PWkpLmVhc2luZyYmKG89bipuKSxcImVhc2VPdXRRdWFkXCI9PT10LmVhc2luZyYmKG89biooMi1uKSksXCJlYXNlSW5PdXRRdWFkXCI9PT10LmVhc2luZyYmKG89bjwuNT8yKm4qbjooNC0yKm4pKm4tMSksXCJlYXNlSW5DdWJpY1wiPT09dC5lYXNpbmcmJihvPW4qbipuKSxcImVhc2VPdXRDdWJpY1wiPT09dC5lYXNpbmcmJihvPS0tbipuKm4rMSksXCJlYXNlSW5PdXRDdWJpY1wiPT09dC5lYXNpbmcmJihvPW48LjU/NCpuKm4qbjoobi0xKSooMipuLTIpKigyKm4tMikrMSksXCJlYXNlSW5RdWFydFwiPT09dC5lYXNpbmcmJihvPW4qbipuKm4pLFwiZWFzZU91dFF1YXJ0XCI9PT10LmVhc2luZyYmKG89MS0gLS1uKm4qbipuKSxcImVhc2VJbk91dFF1YXJ0XCI9PT10LmVhc2luZyYmKG89bjwuNT84Km4qbipuKm46MS04Ki0tbipuKm4qbiksXCJlYXNlSW5RdWludFwiPT09dC5lYXNpbmcmJihvPW4qbipuKm4qbiksXCJlYXNlT3V0UXVpbnRcIj09PXQuZWFzaW5nJiYobz0xKy0tbipuKm4qbipuKSxcImVhc2VJbk91dFF1aW50XCI9PT10LmVhc2luZyYmKG89bjwuNT8xNipuKm4qbipuKm46MSsxNiotLW4qbipuKm4qbiksdC5jdXN0b21FYXNpbmcmJihvPXQuY3VzdG9tRWFzaW5nKG4pKSxvfHxuKSxNLnNjcm9sbFRvKDAsTWF0aC5mbG9vcihkKSksKGZ1bmN0aW9uKGUsdCl7dmFyIG49TS5wYWdlWU9mZnNldDtpZihlPT10fHxuPT10fHwoczx0JiZNLmlubmVySGVpZ2h0K24pPj12KXJldHVybiBDLmNhbmNlbFNjcm9sbCghMCkseChhLHQsYyksSChcInNjcm9sbFN0b3BcIixpLGEsciksIShPPWw9bnVsbCl9KShkLGcpfHwoTz1NLnJlcXVlc3RBbmltYXRpb25GcmFtZShFKSxsPWUpfTswPT09TS5wYWdlWU9mZnNldCYmTS5zY3JvbGxUbygwLDApLGY9YSxoPWksY3x8aGlzdG9yeS5wdXNoU3RhdGUmJmgudXBkYXRlVVJMJiZoaXN0b3J5LnB1c2hTdGF0ZSh7c21vb3RoU2Nyb2xsOkpTT04uc3RyaW5naWZ5KGgpLGFuY2hvcjpmLmlkfSxkb2N1bWVudC50aXRsZSxmPT09ZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50P1wiI3RvcFwiOlwiI1wiK2YuaWQpLFwibWF0Y2hNZWRpYVwiaW4gTSYmTS5tYXRjaE1lZGlhKFwiKHByZWZlcnMtcmVkdWNlZC1tb3Rpb24pXCIpLm1hdGNoZXM/eChhLE1hdGguZmxvb3IoZyksITEpOihIKFwic2Nyb2xsU3RhcnRcIixpLGEsciksQy5jYW5jZWxTY3JvbGwoITApLE0ucmVxdWVzdEFuaW1hdGlvbkZyYW1lKEUpKX19O3ZhciB0PWZ1bmN0aW9uKGUpe2lmKCFlLmRlZmF1bHRQcmV2ZW50ZWQmJiEoMCE9PWUuYnV0dG9ufHxlLm1ldGFLZXl8fGUuY3RybEtleXx8ZS5zaGlmdEtleSkmJlwiY2xvc2VzdFwiaW4gZS50YXJnZXQmJihhPWUudGFyZ2V0LmNsb3Nlc3QobykpJiZcImFcIj09PWEudGFnTmFtZS50b0xvd2VyQ2FzZSgpJiYhZS50YXJnZXQuY2xvc2VzdChiLmlnbm9yZSkmJmEuaG9zdG5hbWU9PT1NLmxvY2F0aW9uLmhvc3RuYW1lJiZhLnBhdGhuYW1lPT09TS5sb2NhdGlvbi5wYXRobmFtZSYmLyMvLnRlc3QoYS5ocmVmKSl7dmFyIHQsbjt0cnl7dD1yKGRlY29kZVVSSUNvbXBvbmVudChhLmhhc2gpKX1jYXRjaChlKXt0PXIoYS5oYXNoKX1pZihcIiNcIj09PXQpe2lmKCFiLnRvcE9uRW1wdHlIYXNoKXJldHVybjtuPWRvY3VtZW50LmRvY3VtZW50RWxlbWVudH1lbHNlIG49ZG9jdW1lbnQucXVlcnlTZWxlY3Rvcih0KTsobj1ufHxcIiN0b3BcIiE9PXQ/bjpkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQpJiYoZS5wcmV2ZW50RGVmYXVsdCgpLChmdW5jdGlvbihlKXtpZihoaXN0b3J5LnJlcGxhY2VTdGF0ZSYmZS51cGRhdGVVUkwmJiFoaXN0b3J5LnN0YXRlKXt2YXIgdD1NLmxvY2F0aW9uLmhhc2g7dD10fHxcIlwiLGhpc3RvcnkucmVwbGFjZVN0YXRlKHtzbW9vdGhTY3JvbGw6SlNPTi5zdHJpbmdpZnkoZSksYW5jaG9yOnR8fE0ucGFnZVlPZmZzZXR9LGRvY3VtZW50LnRpdGxlLHR8fE0ubG9jYXRpb24uaHJlZil9fSkoYiksQy5hbmltYXRlU2Nyb2xsKG4sYSkpfX0sbj1mdW5jdGlvbihlKXtpZihudWxsIT09aGlzdG9yeS5zdGF0ZSYmaGlzdG9yeS5zdGF0ZS5zbW9vdGhTY3JvbGwmJmhpc3Rvcnkuc3RhdGUuc21vb3RoU2Nyb2xsPT09SlNPTi5zdHJpbmdpZnkoYikpe3ZhciB0PWhpc3Rvcnkuc3RhdGUuYW5jaG9yO1wic3RyaW5nXCI9PXR5cGVvZiB0JiZ0JiYhKHQ9ZG9jdW1lbnQucXVlcnlTZWxlY3RvcihyKGhpc3Rvcnkuc3RhdGUuYW5jaG9yKSkpfHxDLmFuaW1hdGVTY3JvbGwodCxudWxsLHt1cGRhdGVVUkw6ITF9KX19O0MuZGVzdHJveT1mdW5jdGlvbigpe2ImJihkb2N1bWVudC5yZW1vdmVFdmVudExpc3RlbmVyKFwiY2xpY2tcIix0LCExKSxNLnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJwb3BzdGF0ZVwiLG4sITEpLEMuY2FuY2VsU2Nyb2xsKCksTz1BPWE9Yj1udWxsKX07cmV0dXJuIChmdW5jdGlvbigpe2lmKCEoXCJxdWVyeVNlbGVjdG9yXCJpbiBkb2N1bWVudCYmXCJhZGRFdmVudExpc3RlbmVyXCJpbiBNJiZcInJlcXVlc3RBbmltYXRpb25GcmFtZVwiaW4gTSYmXCJjbG9zZXN0XCJpbiBNLkVsZW1lbnQucHJvdG90eXBlKSl0aHJvd1wiU21vb3RoIFNjcm9sbDogVGhpcyBicm93c2VyIGRvZXMgbm90IHN1cHBvcnQgdGhlIHJlcXVpcmVkIEphdmFTY3JpcHQgbWV0aG9kcyBhbmQgYnJvd3NlciBBUElzLlwiO0MuZGVzdHJveSgpLGI9SShxLGV8fHt9KSxBPWIuaGVhZGVyP2RvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoYi5oZWFkZXIpOm51bGwsZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsdCwhMSksYi51cGRhdGVVUkwmJmIucG9wc3RhdGUmJk0uYWRkRXZlbnRMaXN0ZW5lcihcInBvcHN0YXRlXCIsbiwhMSl9KSgpLEN9fSkpOyJdLCJuYW1lcyI6W10sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./node_modules/smooth-scroll/dist/smooth-scroll.polyfills.min.js\n");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	!function() {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = function(result, chunkIds, fn, priority) {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var chunkIds = deferred[i][0];
/******/ 				var fn = deferred[i][1];
/******/ 				var priority = deferred[i][2];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every(function(key) { return __webpack_require__.O[key](chunkIds[j]); })) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	!function() {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = function(module) {
/******/ 			var getter = module && module.__esModule ?
/******/ 				function() { return module['default']; } :
/******/ 				function() { return module; };
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	!function() {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = function(exports, definition) {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/global */
/******/ 	!function() {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	!function() {
/******/ 		__webpack_require__.o = function(obj, prop) { return Object.prototype.hasOwnProperty.call(obj, prop); }
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	!function() {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = function(exports) {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	!function() {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"/js/template": 0,
/******/ 			"css/debugbar": 0,
/******/ 			"css/template": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = function(chunkId) { return installedChunks[chunkId] === 0; };
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = function(parentChunkLoadingFunction, data) {
/******/ 			var chunkIds = data[0];
/******/ 			var moreModules = data[1];
/******/ 			var runtime = data[2];
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some(function(id) { return installedChunks[id] !== 0; })) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkId] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunk"] = self["webpackChunk"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	}();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	__webpack_require__.O(undefined, ["css/debugbar","css/template"], function() { return __webpack_require__("./assets/js/template.js"); })
/******/ 	__webpack_require__.O(undefined, ["css/debugbar","css/template"], function() { return __webpack_require__("./assets/scss/template.scss"); })
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["css/debugbar","css/template"], function() { return __webpack_require__("./assets/css/debugbar.css"); })
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	
/******/ })()
;