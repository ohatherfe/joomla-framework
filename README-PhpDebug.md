# PhpDebug Assets! 

The following instructions are exprerimental for [Joomla! Framework](https://framework.joomla.org) 's PhpDebug to align with Laravel's Mix (npm).


## Installation

Follow the instructions for Installation at [README](README.md)

### Webpack

Check the [Webpack](webpack.mix.js) mix file:

*  `mix.autoload` for jQuery and PhpDebugBar 
*  `mix.alias` for webpack directed to Resources files
* `mix.js` combines js assets to media/js/debugbar.js 
* `mix.css` combines css assets to media/css/debugabar.css

## Modifying Resources Assets

The javascript provided from the composer component ('maximefb/debugbar') are not complying with the commonJS module requirements. 
So... a tiny hack is in order...

> ## vendor/maximefb/debugbar/src/Resources/debugbar.js 
> Alteration at the top of the file

> * mix globals: `mixDebugBar` and `jQuery`
> * <s> `PhpDebugBar`</s> as a mix global is a No! no!
 <pre type="javascript">if (typeof(PhpDebugBar) == 'undefined') {
    // namespace
    var PhpDebugBar = mixDebugBar || {};
    PhpDebugBar.$ = jQuery;
}</pre> 
> Add this at the bottom
<pre>
if (typeof (exports) !== 'undefined') {
    module.exports =  PhpDebugBar 
} </pre>

### `save the file ('debugbar.js')`

> run mix dev