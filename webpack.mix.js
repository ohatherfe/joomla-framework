const mix = require('laravel-mix');
require('laravel-mix-purgecss');

// Configure base path for mix stuff going to web
mix.setPublicPath('www/media/');

// Configure base path for media assets
mix.setResourceRoot('/media/');

// Core app JS
mix.js('assets/js/template.js', 'js');

// Core app CSS
mix
    .sourceMaps()
    .sass('assets/scss/template.scss', 'css')
    .options({
        postCss: [
            require('autoprefixer')()
        ]
    })
    .purgeCss({
        extend: {
            content: [
                'templates/**/*.twig',
            ],
        }
    })
;


// PhpDebugBar Assets
mix
    .autoload({
        'jquery': ['$', 'jQuery'],
        '@phpdebug/debugbar': [
            'PhpDebugBar',
            'mixDebugBar'
        ],
    })
    .alias({
        '@phpdebug':   'vendor/maximebf/debugbar/src/DebugBar/Resources'
    })
    .js('assets/js/debugbar.js', 'js')
    .css('assets/css/debugbar.css', 'css', [
        require('postcss-custom-properties')
        // require('postcss-import')
    ])

// Version assets
mix.version();
